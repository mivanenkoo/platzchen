package treads;

import java.util.Random;

public class Enkelkind  implements Runnable{
	
	private String name;
	private Random zuffi;
	

	public Enkelkind(String name) {
		super();
		this.name = name;
	}


	@Override
	public void run() {
		while(true) {
			synchronized (Plaetzchen.class) {
				if(Plaetzchen.getAnzahlKekse()<=20) {
					Plaetzchen.class.notify();
					System.out.println("Oma soll weitere Pl�tzchen backen");
					try {
						Plaetzchen.class.wait();
					}catch(InterruptedException e) {
						e.printStackTrace();
					}
				}
				
				int essen = zuffi.nextInt(20)+5;
				System.out.println("Das Enkelkind hat " + essen + " Pl�tzen gegessen");
				
				Plaetzchen.setAnzahlKekse(Plaetzchen.getAnzahlKekse()-essen);
				System.out.println("In der Dose sind " + Plaetzchen.getAnzahlKekse() + " Pl�tzhen");
				
			}
			try {
				Thread.sleep(800);
			}catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}

	

}
