package treads;

import java.util.Random;

public class Oma implements Runnable{

	private Random zuffi;
	
	public Oma() {
		zuffi=new Random();
	}
	
	
	
	@Override
	public void run() {

		while (true) {
	
			synchronized (Plaetzchen.class) {
				
				// Zuf�llige Zahl an  Pl�tzchen  backen
				int gebacken = zuffi.nextInt(20)+5;
				System.out.println("Oma hat " + gebacken + " Pl�tzchen gebacken");
				
				// Diese Anzahl in die Dose legen
				Plaetzchen.kekseInDieDose(gebacken);
				System.out.println("In der Dose sind jetzt " + Plaetzchen.getAnzahlKekse() + " Pl�tzchen");
				
				// Sobald in der Doze mehr als 100 Pl�tzchen sind, sollen die Enkelkinder die essen.
				if(Plaetzchen.getAnzahlKekse()>=100) {
					Plaetzchen.class.notifyAll(); //->Sendet Signal an alle Enkelkinder
					
					try {
						Plaetzchen.class.wait();
					}catch(InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
			
			try {
				Thread.sleep(800);
			}catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
		
	}

}
