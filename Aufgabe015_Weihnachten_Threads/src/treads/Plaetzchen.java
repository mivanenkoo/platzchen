package treads;

public class Plaetzchen {
	
	private static int anzahlKekse;

	public static int getAnzahlKekse() {
		return anzahlKekse;
	}

	public static void setAnzahlKekse(int anzahl) {
		anzahlKekse = anzahl;
	}
	
	public static void esseKekse(int anzahl) {
//		this.anzahlKekse=anzahl;
		anzahlKekse-=anzahl;
	}
	
	public static void kekseInDieDose(int anzahl) {
		anzahlKekse+=anzahl;
	}

}
